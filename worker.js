const Cart = require('./carro.js')
const zmq = require('zeromq')
let req = zmq.socket('req')

req.identity = 'Worker1' + process.pid

req.connect('tcp://localhost:9999')

req.on('message', (c, sep, envio) => {
    var envioAux =JSON.parse(envio);


    let objetoArray =  envioAux.objson.items;
    var carro = new Cart(objetoArray);

    //console.log(envioAux.objson.items);

    if(envioAux.action === 'add') {
         carro.add(envioAux.prod);
       // console.log(carro.items);
    }
    if(envioAux.action === 'remove') {
        carro.remove(envioAux.prod);
        console.log(carro.items);
    }

    setTimeout(() => {
        req.send([c, '', JSON.stringify(carro)])
    }, 1000)
})
req.send(['', '', ''])